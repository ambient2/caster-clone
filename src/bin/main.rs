use caster_clone::*;
use image::DynamicImage;

pub fn main() -> anyhow::Result<()> {
    let scene = Scene {
        width: 800,
        height: 600,
        fov: 90.0,
        elements: vec![
            Element::Sphere(Sphere {
                center: Point {
                    x: 0.0,
                    y: 0.0,
                    z: -5.0,
                },
                radius: 1.0,
                color: Color {
                    red: 0.4,
                    green: 1.0,
                    blue: 0.4,
                },
                albedo: 0.50,
            }),
            Element::Plane(Plane {
                origin: Point::new(0.0, -2.0, -5.0),
                normal: Vector3::new(0.0, -1.0, 0.0).normalize(),
                color: Color {
                    red: 0.4,
                    green: 0.4,
                    blue: 0.4,
                },
                albedo: 0.50,
            }),
            Element::Sphere(Sphere {
                center: Point {
                    x: 5.0,
                    y: 5.0,
                    z: -10.0,
                },
                radius: 1.5,
                color: Color {
                    red: 1.0,
                    green: 1.0,
                    blue: 0.0,
                },
                albedo: 0.50,
            }),
        ],
        lights: vec![Light {
            direction: Vector3::new(-0.5, 1.0, 0.2),
            color: Color {
                red: 1.0,
                green: 1.0,
                blue: 1.0,
            },
            intensity: 1.0,
        }],
        shadow_bias: 1e-6,
    };

    let img: DynamicImage = render(&scene);
    assert_eq!(scene.width, img.as_rgb8().unwrap().width());
    assert_eq!(scene.height, img.as_rgb8().unwrap().height());
    img.save_with_format("test.png", image::ImageFormat::Png)
        .unwrap();
    Ok(())
}
