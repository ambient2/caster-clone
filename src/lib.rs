use image::{DynamicImage, GenericImage, Pixel, Rgba};

const GAMMA: f32 = 2.2;

fn gamma_encode(linear: f32) -> f32 {
    linear.powf(1.0 / GAMMA)
}

pub trait Normalizable {
    fn surface_n(&self, intersection: Point) -> Vector3;
}

impl Normalizable for Sphere {
    fn surface_n(&self, intersection: Point) -> Vector3 {
        (intersection - self.center).normalize()
    }
}

impl Normalizable for Plane {
    fn surface_n(&self, _intersection: Point) -> Vector3 {
        self.normal.scale(-1.0)
    }
}

pub trait AlbedoRef {
    fn albedo(&self) -> f32;
}

impl AlbedoRef for Plane {
    fn albedo(&self) -> f32 {
        self.albedo
    }
}

impl AlbedoRef for Sphere {
    fn albedo(&self) -> f32 {
        self.albedo
    }
}

pub trait Intersectable {
    fn intersect(&self, ray: &Ray) -> Option<f64>;
}

impl Intersectable for Sphere {
    fn intersect(&self, ray: &Ray) -> Option<f64> {
        let l = self.center - ray.origin;
        let adj = l * ray.direction;
        let d2 = l * l - adj * adj;
        let radius2 = self.radius * self.radius;
        if d2 > radius2 {
            return None;
        }
        let thc = (radius2 - d2).sqrt();
        let t0 = adj - thc;
        let t1 = adj + thc;

        if t0 < 0.0 && t1 < 0.0 {
            return None;
        }

        let distance = if t0 < t1 { t0 } else { t1 };
        Some(distance)
    }
}

#[derive(Clone, Copy)]
pub struct Plane {
    pub origin: Point,
    pub normal: Vector3,
    pub color: Color,
    pub albedo: f32,
}

impl Intersectable for Plane {
    fn intersect(&self, ray: &Ray) -> Option<f64> {
        let norm = self.normal;
        let denom = norm * ray.direction;
        if denom > 1e-6 {
            let v = self.origin - ray.origin;
            let distance = v * norm / denom;
            if distance >= 0.0 {
                return Some(distance);
            }
        }
        None
    }
}

#[derive(Clone, Copy)]
pub enum Element {
    Sphere(Sphere),
    Plane(Plane),
}

impl Element {
    pub fn color(&self) -> Color {
        match &self {
            Element::Plane(ref p) => p.color,
            Element::Sphere(ref s) => s.color,
        }
    }
}

impl Intersectable for Element {
    fn intersect(&self, ray: &Ray) -> Option<f64> {
        match &self {
            Element::Plane(ref p) => p.intersect(ray),
            Element::Sphere(ref s) => s.intersect(ray),
        }
    }
}

impl Normalizable for Element {
    fn surface_n(&self, intersection: Point) -> Vector3 {
        match &self {
            Element::Plane(ref p) => p.surface_n(intersection),
            Element::Sphere(ref s) => s.surface_n(intersection),
        }
    }
}

impl AlbedoRef for Element {
    fn albedo(&self) -> f32 {
        match &self {
            Element::Plane(ref p) => p.albedo(),
            Element::Sphere(ref s) => s.albedo(),
        }
    }
}

#[derive(Clone, Copy)]
pub struct Intersection<'a> {
    pub distance: f64,
    pub object: &'a Element,
}

impl<'a> Intersection<'a> {
    pub fn new(distance: f64, object: &'a Element) -> Intersection<'a> {
        Intersection { distance, object }
    }
}

#[derive(Clone, Copy)]
pub struct Ray {
    pub origin: Point,
    pub direction: Vector3,
}

impl Ray {
    pub fn create_prime(x: u32, y: u32, scene: &Scene) -> Ray {
        let s_x: f64 = ((x as f64 + 0.5) as f64 / scene.width as f64) * 2.0 - 1.0;
        let s_y: f64 = 1.0 - ((y as f64 + 0.5) / scene.height as f64) * 2.0;

        let aspect_ratio = scene.width as f64 / scene.height as f64;
        let s_x = s_x * aspect_ratio;

        let fov_adjustment = (scene.fov.to_radians() / 2.0).tan();
        let s_x = s_x * fov_adjustment;
        let s_y = s_y * fov_adjustment;

        let direction = Vector3::new(s_x, s_y, -1.0).normalize();
        Ray {
            origin: Point::zero(),
            direction,
        }
    }
}
#[derive(Clone, Copy)]
pub struct Vector3 {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl Vector3 {
    pub fn zero() -> Vector3 {
        Vector3 {
            x: 0.0,
            y: 0.0,
            z: 0.0,
        }
    }

    pub fn new(x: f64, y: f64, z: f64) -> Vector3 {
        Vector3 { x, y, z }
    }

    pub fn normalize(&self) -> Vector3 {
        let magn_squared = self.x.powi(2) + self.y.powi(2) + self.z.powi(2);
        if magn_squared == 0.0 {
            Vector3::zero()
        } else {
            let m = magn_squared.sqrt();
            let x = self.x / m;
            let y = self.y / m;
            let z = self.z / m;
            Vector3::new(x, y, z)
        }
    }

    pub fn scale(&self, factor: f64) -> Vector3 {
        let x = self.x * factor;
        let y = self.y * factor;
        let z = self.z * factor;
        Vector3 { x, y, z }
    }
}

impl std::ops::Sub for Vector3 {
    type Output = Vector3;

    fn sub(self, rhs: Self) -> Self::Output {
        let Vector3 {
            x: l_x,
            y: l_y,
            z: l_z,
        } = self;
        let Vector3 {
            x: r_x,
            y: r_y,
            z: r_z,
        } = rhs;
        Vector3::new(l_x - r_x, l_y - r_y, l_z - r_z)
    }
}

impl std::ops::Mul for Vector3 {
    type Output = f64;

    fn mul(self, rhs: Self) -> Self::Output {
        let Vector3 {
            x: l_x,
            y: l_y,
            z: l_z,
        } = self;
        let Vector3 {
            x: r_x,
            y: r_y,
            z: r_z,
        } = rhs;
        (l_x * r_x) + (l_y * r_y) + (l_z * r_z)
    }
}
#[derive(Clone, Copy, PartialEq, PartialOrd, Debug)]
pub struct Color {
    pub red: f32,
    pub green: f32,
    pub blue: f32,
}

impl Color {
    pub fn to_rgba(&self) -> Rgba<u8> {
        Rgba::from_channels(
            (gamma_encode(self.red) * 255.0) as u8,
            (gamma_encode(self.green) * 255.0) as u8,
            (gamma_encode(self.blue) * 255.0) as u8,
            255,
        )
    }

    pub fn scale(&self, factor: f32) -> Color {
        let Color { red, blue, green } = self;
        Color {
            red: red * factor,
            blue: blue * factor,
            green: green * factor,
        }
    }

    pub fn clamp(&self) -> Color {
        Color {
            red: self.red.min(1.0).max(0.0),
            blue: self.blue.min(1.0).max(0.0),
            green: self.green.min(1.0).max(0.0),
        }
    }
}

#[derive(Clone, Copy)]
pub struct Point {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

impl Point {
    pub fn new(x: f32, y: f32, z: f32) -> Point {
        Point { x, y, z }
    }

    pub fn zero() -> Point {
        Point::new(0.0, 0.0, 0.0)
    }
}

impl std::ops::Sub for Point {
    type Output = Vector3;

    fn sub(self, rhs: Self) -> Self::Output {
        let Point {
            x: l_x,
            y: l_y,
            z: l_z,
        } = self;
        let Point {
            x: r_x,
            y: r_y,
            z: r_z,
        } = rhs;
        Vector3::new((l_x - r_x).into(), (l_y - r_y).into(), (l_z - r_z).into())
    }
}

#[derive(Clone, Copy)]
pub struct Sphere {
    pub center: Point,
    pub radius: f64,
    pub color: Color,
    pub albedo: f32,
}

#[derive(Clone, Copy)]
pub struct Light {
    pub direction: Vector3,
    pub color: Color,
    pub intensity: f32,
}

pub struct Scene {
    pub width: u32,
    pub height: u32,
    pub fov: f64,
    pub elements: Vec<Element>,
    pub lights: Vec<Light>,
    pub shadow_bias: f64,
}

impl Scene {
    pub fn trace(&self, ray: &Ray) -> Option<Intersection> {
        self.elements
            .iter()
            .filter_map(|s| s.intersect(ray).map(|d| Intersection::new(d, s)))
            .min_by(|i1, i2| i1.distance.partial_cmp(&i2.distance).unwrap())
    }
}

impl std::ops::Mul for Color {
    type Output = Color;

    fn mul(self, rhs: Self) -> Self::Output {
        let Color { red, blue, green } = self;
        let Color {
            red: red2,
            blue: blue2,
            green: green2,
        } = rhs;
        Color {
            red: red * red2,
            blue: blue * blue2,
            green: green * green2,
        }
    }
}

impl std::ops::Add for Color {
    type Output = Color;

    fn add(self, rhs: Self) -> Self::Output {
        Color {
            red: self.red + rhs.red,
            green: self.green + rhs.green,
            blue: self.blue + rhs.blue,
        }
    }
}

pub fn render(scene: &Scene) -> DynamicImage {
    let mut image = DynamicImage::new_rgb8(scene.width, scene.height);
    let black: Rgba<u8> = Rgba::from_channels(0, 0, 0, 0);
    for x in 0..scene.width {
        for y in 0..scene.height {
            image.put_pixel(x, y, black);
            let ray = Ray::create_prime(x, y, scene);
            let i = scene.trace(&ray);
            if i.is_none() {
                continue;
            }

            let i = i.unwrap();

            let iscale = ray.direction.scale(i.distance);
            let hit_point = Point::new(iscale.x as f32, iscale.y as f32, iscale.z as f32);
            let hit_point = Point::new(
                hit_point.x + ray.origin.x,
                hit_point.y + ray.origin.y,
                hit_point.z + ray.origin.z,
            );

            let s_n = i.object.surface_n(hit_point);
            let mut color = Color {
                red: 0.0,
                blue: 0.0,
                green: 0.0,
            };
            for light in scene.lights.iter() {
                let light_dir = light.direction;

                let offset = s_n.scale(scene.shadow_bias);
                let shadow_ray = Ray {
                    origin: Point::new(
                        hit_point.x + offset.x as f32,
                        hit_point.y + offset.y as f32,
                        hit_point.z + offset.z as f32,
                    ),
                    direction: light_dir,
                };
                let in_light = scene.trace(&shadow_ray).is_none();

                let light_intensity = if in_light { light.intensity } else { 0.0 };
                let light_power = (s_n * light_dir).max(0.0) as f32 * light_intensity;
                let light_reflected = i.object.albedo() / std::f32::consts::PI;

                let light_color = light.color.scale(light_power * light_reflected);

                color = color + (i.object.color() * light_color);
            }

            for e in scene.elements.iter() {
                if e.intersect(&ray).is_some() {
                    image.put_pixel(x, y, color.to_rgba());
                }
            }
        }
    }
    image
}
