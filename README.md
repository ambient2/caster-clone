# caster-clone

## Current State

![current_state](images/current_state.png)

## Multiple Lights & Objects

![multiple_entities](images/multiple_lights.png)

## Shadow Aliasing

![shadow_aliasing](images/shadow_aliasing.png)
